library(ggplot2)

# plot
# Default call (as object)
p <- ggplot(data, aes(x=valence,y=arousal))+ 
  stat_bin2d(bins=12)
p



wout <- subset(data, outlier == 1)

ggplot(wout, aes(x, y, fill = z)) + geom_raster()

p <- ggplot(wout, aes(x=valence,y=arousal))+
  stat_bin2d(bins=6) 
p


p <- ggplot(data, aes(x=intense,y=arousal)) +geom_point()
p


h3 <- p + stat_bin2d(bins=25) + scale_fill_gradientn(colours=r)
h3





# �a marche pas :


library(akima)
resolution <- 0.1 # you can increase the resolution by decreasing this number (warning: the resulting dataframe size increase very quickly)
a <- interp(x=data$valence, y=data$arousal, z=data$intense, 
            xo=seq(min(data$valence),max(data$arousal),by=resolution), 
            yo=seq(min(data$arousal),max(data$valence),by=resolution), duplicate="mean")
image(a) #you can of course modify the color palette and the color categories. See ?image for more explanation
