library("FactoMineR")
library("factoextra")






## Correpondance matrix 

library("gplots")
data(dataMedianPerTrialTotal)


caData = dataMedianPerTrialTotal[, c("str")]
row.names(caData) <- dataMedianPerTrialTotal$str
caData


dt <- as.table(as.matrix(caData))
# 2. Graph
balloonplot(t(dt), main ="caData", xlab ="", ylab="",
            label = TRUE, show.margins = FALSE)

 # "Discriminant correspondence analysis."

### MCA plot of variabls 

# Not working


d <- caData
d$arousal =   as.factor(d$arousal)
d$valence =   as.factor(d$valence)

mca1 = MCA(caData, graph = FALSE)
mca1
mca1$eig


mca1_vars_df = data.frame(mca1$ind$coord, Variable = rep(names(cats), cats))

# data frame with observation coordinates
mca1_obs_df = data.frame(mca1$ind$coord)

# plot of variable categories
ggplot(data=mca1_obs_df, 
       aes(x = Dim.1, y = Dim.2, label = rownames(mca1_obs_df))) +
  geom_hline(yintercept = 0, colour = "gray70") +
  geom_vline(xintercept = 0, colour = "gray70") +
  geom_point( size = 3) +
  geom_text(nudge_y=-0.1) +
  ggtitle("MCA plot of variables using R package FactoMineR")



### Graphs PARAMETERS




data(dataMedianPerTrialTotal)
caData = dataMedianPerTrialTotal[, c("speed", "force","repetition","distance")]
row.names(caData) <- dataMedianPerTrialTotal$str

d <- caData
d$speed =   as.factor(d$speed)
d$force =   as.factor(d$force)
d$repetition =   as.factor(d$repetition)
d$distance =   as.factor(d$distance)


cats = apply(d, 1, function(x) nlevels(as.factor(x)))
cats



mca1 = MCA(d, graph = FALSE)
mca1
mca1$eig


mca1_vars_df = data.frame(mca1$var$coord, Variable = rep(names(cats), cats))

# data frame with observation coordinates
mca1_obs_df = data.frame(mca1$ind$coord)

# plot of variable categories
ggplot(data=mca1_vars_df, 
       aes(x = Dim.1, y = Dim.2, label = rownames(mca1_vars_df))) +
  geom_hline(yintercept = 0, colour = "gray70") +
  geom_vline(xintercept = 0, colour = "gray70") +
  geom_text(aes(colour=Variable)) +
  ggtitle("MCA plot of variables using R package FactoMineR")




ggplot(data = mca1_obs_df, aes(x = Dim.1, y = Dim.2)) +
  geom_hline(yintercept = 0, colour = "gray70") +
  geom_vline(xintercept = 0, colour = "gray70") +
  geom_point(colour = "gray50", alpha = 0.7) +
  geom_density2d(colour = "gray80") +
  geom_text(data = mca1_vars_df, 
            aes(x = Dim.1, y = Dim.2, 
                label = rownames(mca1_vars_df), colour = Variable)) +
  ggtitle("MCA plot of variables using R package FactoMineR") +
  scale_colour_discrete(name = "Variable")





d.ca = CA(d)

plot.CA(d, axes = c(1,2), col.row = "blue", col.col = "red")






















plot.CA(caData, axes = c(1,2), col.row = "blue", col.col = "red")


res.ca <- CA (as.matrix(caData))
## select rows and columns that have a cos2 greater than 0.8
plot(res.ca,selectCol="cos2 0.8",selectRow="cos2 0.8")






cats = as.data.frame(apply(caData, 2, function(x) nlevels(as.factor(x))))

mca1 = MCA(dataMedianPerTrialTotal, graph = FALSE)

# list of results
mca1







mca1 = MCA(cats, graph = TRUE)
mca1 <- MCA(data, graph = TRUE)

# list of results
mca1








###" Other method :

# Correspondence Analysis
library(ca)
mytable <- with(data, table(speed,repetition)) # create a 2 way table
prop.table(mytable, 1) # row percentages
prop.table(mytable, 2) # column percentages
fit <- ca(mytable)
print(fit) # basic results 
summary(fit) # extended results 
plot(fit) # symmetric map
plot(fit, mass = TRUE, contrib = "absolute", map =
       "rowgreen", arrows = c(FALSE, TRUE)) # asymmetric map


data("wg93")

summary(mjca(data[,c("arousal","valence")], lambda = "Burt"))

