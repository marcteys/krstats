

  # same F statistics as single-df terms



### Traite les donn?es par user



rhg_cols <- c("#e6acd2","#ff80c4","#733967","#ff7340","#ff0066","#66000e","#def2b6","#592d2d","#e57373","#594c16","#2b0040","#bf001a","#858c69","#535ea6","#e5a173","#3df2b6","#1d00d9","#00294d","#8c6973","#aa66cc","#1a3133","#8c3f23","#20f200","#114000","#b4ace6","#467e8c","#3399cc","#f2ff40","#0066ff","#e57a00","#434959","#4c3626","#401029","#cca300","#f200e2","#6b7300","#73dee6","#468c6c","#3aa629","#990052","#002999")
rhg_cols2 <- c("#e57373","#cca300","#20f200","#3399cc","#1d00d9","#990052","#592d2d","#594c16","#3aa629","#00294d","#aa66cc","#401029","#ff7340","#f2ff40","#468c6c","#0066ff","#2b0040","#ff0066","#8c3f23","#6b7300","#3df2b6","#002999","#f200e2","#8c6973","#e5a173","#858c69","#73dee6","#434959","#733967","#bf001a","#4c3626","#def2b6","#1a3133","#535ea6","#e6acd2","#66000e","#e57a00","#114000","#467e8c","#b4ace6","#ff80c4")
colors37 = c("#466791","#60bf37","#953ada","#4fbe6c","#ce49d3","#a7b43d","#5a51dc","#d49f36","#552095","#507f2d","#db37aa","#84b67c","#a06fda","#df462a","#5b83db","#c76c2d","#4f49a3","#82702d","#dd6bbb","#334c22","#d83979","#55baad","#dc4555","#62aad3","#8c3025","#417d61","#862977","#bba672","#403367","#da8a6d","#a79cd4","#71482c","#c689d0","#6b2940","#d593a7","#895c8b","#bd5975")

n <- 16
qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
col_vector = unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))




## Basic graphs 
graphTouchersByUsers <- ggplot(dataStaticPlusId, aes(x=valence, y=arousal, color=str)) +
  geom_point(aes(colour=str)) + facet_wrap(~ subject, shrink = FALSE)+
  scale_colour_manual(values=rhg_cols2) +
  geom_hline(yintercept=0) + geom_vline(xintercept=0) +
  # theme(axis.line.x = element_line(color="black", size =1, ylim(1,3)),
  #      axis.line.y = element_line(color="black", size = 1)) +
  scale_x_continuous(expand=c(0,0)) +
  scale_y_continuous(expand=c(0,0)) +
  labs(colour = "Touchers") +
  theme(strip.text.x = element_text(size = 8, colour = "black"), strip.background = element_blank())
graphTouchersByUsers




graphUsersbyTouchers <- ggplot(dataStaticPlusId, aes(x=valence, y=arousal, color=subject)) +
  geom_point(aes(colour=subject)) + facet_wrap(~ str, shrink = FALSE)+
  scale_colour_manual(values=rhg_cols2) +
  geom_hline(yintercept=0) + geom_vline(xintercept=0) +
  # theme(axis.line.x = element_line(color="black", size =1, ylim(1,3)),
  #      axis.line.y = element_line(color="black", size = 1)) +
  scale_x_continuous(expand=c(0,0)) +
  scale_y_continuous(expand=c(0,0)) +
  labs(colour = "Users") +
  theme(strip.text.x = element_text(size = 8, colour = "black"), strip.background = element_blank())
graphUsersbyTouchers



## Generate plotLy
graphTouchersByUsers <- ggplotly(graphTouchersByUsers)
graphTouchersByUsers


graphUsersbyTouchers <- ggplotly(graphUsersbyTouchers)
graphUsersbyTouchers



api_create(graphUsersbyTouchers, filename = "KRS Users by touchers",fileopt = "overwrite", sharing = "public")
api_create(graphTouchersByUsers, filename = "KRS Touchers by Users",fileopt = "overwrite", sharing = "public")






####################" STATIC





## Graph en excluant ou incluant les usersqui aiment etre touch?s 

dataStaticPlusSensitive = merge(dataStaticPlusId, users, by=("subject"))
dataStaticPlusSensitive$likeTacileGroup <- as.factor((dataStaticPlusSensitive$LikeTactile >3.5))

dataStaticPlusSensitive$isTactileGroup <- as.factor((dataStaticPlusSensitive$IsTactile >3.5))



graphUsersSensitivebyTouchers <- ggplot(dataStaticPlusSensitive, aes(x=valence, y=arousal)) +
  geom_point(aes(colour=likeTacileGroup)) + facet_wrap(~ str, shrink = FALSE)+
  scale_colour_manual(values=rhg_cols2) +
  geom_hline(yintercept=0) + geom_vline(xintercept=0) +
  # theme(axis.line.x = element_line(color="black", size =1, ylim(1,3)),
  #      axis.line.y = element_line(color="black", size = 1)) +
  scale_x_continuous(expand=c(0,0)) +
  scale_y_continuous(expand=c(0,0)) +
  labs(colour = "Utilisateur sensible") +
  theme(strip.text.x = element_text(size = 8, colour = "black"), strip.background = element_blank())
graphUsersSensitivebyTouchers



graphTouchersByUsersSensitive <- ggplot(dataStaticPlusSensitive, aes(x=valence, y=arousal, color=str)) +
  geom_point(aes(colour=likeTacileGroup)) + facet_wrap(~ subject, shrink = FALSE)+
  scale_colour_manual(values=rhg_cols2) +
  geom_hline(yintercept=0) + geom_vline(xintercept=0) +
  # theme(axis.line.x = element_line(color="black", size =1, ylim(1,3)),
  #      axis.line.y = element_line(color="black", size = 1)) +
  scale_x_continuous(expand=c(0,0)) +
  scale_y_continuous(expand=c(0,0)) +
  labs(colour = "Utilisateur sensible") +
  theme(strip.text.x = element_text(size = 8, colour = "black"), strip.background = element_blank())
graphTouchersByUsersSensitive





graphTouchersByUsers <- ggplotly(graphTouchersByUsers)
graphTouchersByUsers



graphUsersbyTouchers <- ggplotly(graphUsersbyTouchers)
graphUsersbyTouchers




#api_create(graphUsersbyTouchers, filename = "KRS Users by touchers",fileopt = "overwrite", sharing = "public")
#api_create(graphTouchersByUsers, filename = "KRS Touchers by Users",fileopt = "overwrite", sharing = "public")




