library("ggplot2")
library("plotly")

df = read.table(text = 
                  "id      year    value1  value2 value3
            1           2000    1   2000      2001
            1           2001    2   NA        NA
            1           2002    2   2000      NA
            1           2003    2   NA         2003
            2           2000    1   2001     2003
            2           2002    2   NA       2000
            2           2003    3   2002     NA
            3           2002    2   2001     NA
        ", sep = "", header = TRUE)
df$value1 <- as.factor(df$value1)

p <- ggplot(df, aes(y=id)) +
  geom_point(aes(x=year, color=value1), size=4) +
  geom_point(aes(x=value3, colour ='value3'), size=3) +
  geom_point(aes(x=value2, colour ='value2'), size=5) +
  geom_hline(yintercept=0) + geom_vline(xintercept=0) +
  scale_colour_manual(name="",  
                      values = c("1"="yellow", "2"="orange", "3"="red",
                                 "value3"="grey", "value2"="black"))


ply <- ggplotly(p)
ply
