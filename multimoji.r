library(ggplot2)
library(ddply)
library(plotly)
Sys.setenv("plotly_username"="marcteys")
Sys.setenv("plotly_api_key"="wvqv7wjjpZHxVcrI0Gn4")



multimoj <- read.csv("MultiMoji Data/vibrotermal.csv",sep=",",header= TRUE)
multimoj <- read.csv("MultiMoji Data/multimoj.csv",sep=",",header= TRUE)
multimoj <- read.csv("MultiMoji Data/visualtermal.csv",sep=",",header= TRUE)

multimoj$arousal = multimoj$arousal - 4
multimoj$valence = multimoj$valence - 4

multiMojEllipse = ggplot(multimoj, aes_string(x = "valence", y="arousal", color="stimuli"))  +
  geom_point(aes(x=valence, y=arousal),size=1) +
  stat_ellipse(aes(x=valence, y=arousal), level = 0.95) +
  coord_cartesian(ylim=c(-3,3),xlim=c(-3,3) )

multiMojEllipse

## Generate plotLy
multiMojEllipse <- ggplotly(multiMojEllipse)
multiMojEllipse


api_create(multiMojEllipse, filename = "multiMojEllipse",fileopt = "overwrite", sharing = "public")





### Mean

library(plyr)
library(ggplot2)
multimojMean = ddply(multimoj, .(stimuli), summarize,  arousal=mean(arousal), valence=mean(valence))



multimojMeanGraph = ggplot(multimojMean, aes_string(x = "valence", y="arousal", color="stimuli"))  +
  geom_point(aes(x=valence, y=arousal),size=1) +
  coord_cartesian(ylim=c(-3,3),xlim=c(-3,3) )

multimojMeanGraph

## Generate plotLy
multimojMeanGraph <- ggplotly(multimojMeanGraph)
multimojMeanGraph


api_create(multimojMeanGraph, filename = "MultiMean",fileopt = "overwrite", sharing = "public")


