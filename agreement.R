



saticici = subset(dataStatic, outlier ==1)

kruskal.test(force ~ Calm, data = saticici) 
kruskal.test(force ~ Disgust , data = saticici) 
kruskal.test(force ~ Angry, data = saticici)
kruskal.test(force ~ Sad  , data = saticici)
kruskal.test(force~ Happy  , data = saticici)
kruskal.test(force~ Fear  , data = saticici)
kruskal.test(repetition~ Surprise  , data = saticici)
kruskal.test(force~ Surprise , data = saticici)






aaaa = subset(data, outlier ==1)

lowSpeed = subset(aaaa, speed == 3.8)
highSpeed = subset(aaaa, speed == 16)
sum(highSpeed$Angry / 552)
sum(lowSpeed$Angry / 565)
sum(lowSpeed$Sad / 565)
sum(highSpeed$Sad / 552)
sum(lowSpeed$Calm / 565)
sum(highSpeed$Calm / 552)

lowA = subset(aaaa, amplitude == 5)
highA = subset(aaaa, amplitude == 20)
sum(lowA$Happy / 558)
sum(highA$Happy / 559)

sum(lowA$Calm / 558)
sum(highA$Calm / 559)




kruskal.test(speed ~ Calm, data = data) 


kruskal.test(amplitude ~ Calm, data = aaaa) 
kruskal.test(amplitude ~ Disgust , data = aaaa) 
kruskal.test(amplitude ~ Angry, data = aaaa)
kruskal.test(amplitude ~ Sad  , data = aaaa)
kruskal.test(amplitude~ Happy  , data = aaaa)
kruskal.test(amplitude~ Fear  , data = aaaa)
kruskal.test(amplitude~ Interest  , data = data)
kruskal.test(amplitude~ Surprise , data = data)

kruskal.test( speed ~ Angry, data = aaaa)


kruskal.test(Surprise+ Fear + Interest + Sad + Happy ~ amplitude, data = aaaa)



data$Disgust = ifelse(data$emotionIndex == 0 , 1, 0) 
data$Angry = ifelse(data$emotionIndex == 1 , 1, 0) 
data$Sad = ifelse(data$emotionIndex == 2 , 1, 0) 
data$Happy = ifelse(data$emotionIndex == 3 , 1, 0) 
data$Fear = ifelse(data$emotionIndex == 4 , 1, 0) 
data$Interest = ifelse(data$emotionIndex == 5 , 1, 0) 
data$IDK = ifelse(data$emotionIndex == 6 , 1, 0) 
data$Calm = ifelse(data$emotionIndex == 7 , 1, 0) 
data$Angry = ifelse(data$emotionIndex == 8 , 1, 0) 

dataStatic$Disgust = ifelse(dataStatic$emotionIndex == 0 , 1, 0) 
dataStatic$Angry = ifelse(dataStatic$emotionIndex == 1 , 1, 0) 
dataStatic$Sad = ifelse(dataStatic$emotionIndex == 2 , 1, 0) 
dataStatic$Happy = ifelse(dataStatic$emotionIndex == 3 , 1, 0) 
dataStatic$Fear = ifelse(dataStatic$emotionIndex == 4 , 1, 0) 
dataStatic$Interest = ifelse(dataStatic$emotionIndex == 5 , 1, 0) 
dataStatic$IDK = ifelse(dataStatic$emotionIndex == 6 , 1, 0) 
dataStatic$Calm = ifelse(dataStatic$emotionIndex == 7 , 1, 0) 
dataStatic$Angry = ifelse(dataStatic$emotionIndex == 8 , 1, 0) 







ccc = data[ , c("str", "strWORep","force", "speed", "amplitude",  "Disgust", "Angry", "Sad", "Happy", "Fear", "outlier", "Interest", "IDK", "Calm", "Angry")]
write.csv(ccc, file = "dataEmotions.csv")
kruskal.test(Calm ~ force, data = data) 
kruskal.test(Calm ~ force, data = data) 
kruskal.test(Calm ~ force, data = data) 


kruskal.test(emotion ~ repetition, data = data) 
kruskal.test(emotion ~ amplitude, data = data) 
kruskal.test(emotion ~ speed, data = data) 




###########################################################################






arating <- dataMeanPerTrial

arating$isArousalLess <- ifelse(arating$arousal <= -1 , 1, 0) 
arating$isArousalMore <- ifelse(arating$arousal >= 1 , 1, 0) 
arating$isValenceLess <- ifelse(arating$valence <= -1 , 1, 0) 
arating$isValenceMore <- ifelse(arating$valence >= 1 , 1, 0) 
sum(arating$isValenceMore + arating$isValenceLess)
sum(arating$isArousalMore + arating$isArousalLess)

for (i in 1:8 ) {
  agree <-arating[which(arating$indiceWORep == i),]
  print(paste(unique(agree$strWORep)," valence more:", sum(agree$isValenceMore / 48 ) *100 ))
  print(paste(unique(agree$strWORep)," valence less:", sum(agree$isValenceLess / 48 ) *100 ))
  print(paste(unique(agree$strWORep)," arousal more:", sum(agree$isArousalMore / 48 ) *100 ))
  print(paste(unique(agree$strWORep)," arousal less:", sum(agree$isArousalLess / 48 ) *100 ))
  print("----")
}


test <- arating[which(arating$strWORep == "V+A-P+"),]
sum(test$isArousalLess + test$isArousalMore)
sum()

p <- function(newdata) {
  print(newdata$isValenceMore)
}







### Agreement same stimuli



agreementPercent2 = data.frame()

for (i in 1:24 ) {
  agree <- Agreement.Percent(data[which(data$indice ==i & data$outlier == 1),])
  agreementPercent2 <- rbind(agreementPercent2, agree[2,])
}


agreementPercent = data.frame()

for (i in 1:8 ) {
  agree <- Agreement.PercentWORep(data[which(data$indiceWORep ==i & data$outlier == 1),])
  agreementPercent <- rbind(agreementPercent, agree[2,])
  
  t = agreementPercent2[i:(i+2),]
  m  = sum(t)
  print(t)
  meanM = m - agree[2,]
 # print(m)
}

agreementPercent
agreementPercent[, "max"] <- apply(agreementPercent[,1:8], 1, max)
summary(agreementPercent)
sd(agreementPercent[,"max"])






Agreement.Percent <- function(newdata) {
  
  count <- data.frame(table(unlist(newdata$emotion)))
  
  count <- count[count$Var1 != "I don't know", ] # We remove the I don't know
  count$percent = round((count$Freq / sum(count$Freq)),3)
  colnames(count)[1] <- "Emotion"
  newRow<- data.frame(Emotion="Total",Freq=sum(count$Freq), percent = sum(count$percent))
  count <- rbind(count, newRow)
  colnames(count)[3] <- paste(unique(newdata$str),"")
  
  extCount <- data.frame(t(count[-1]))
  colnames(extCount) <- count[, 1]
  
  return(extCount)
}

agreementPercent = data.frame()

for (i in 1:16 ) {
  agree <- Agreement.Percent(data[which(data$indice ==i & data$outlier == 1),])
  agreementPercent <- rbind(agreementPercent, agree[2,])
}




Agreement.PercentWORep <- function(newdata) {
  
  count <- data.frame(table(unlist(newdata$emotion)))
  
  count <- count[count$Var1 != "I don't know", ] # We remove the I don't know
  count$percent = round((count$Freq / sum(count$Freq)),3)
  colnames(count)[1] <- "Emotion"
  newRow<- data.frame(Emotion="Total",Freq=sum(count$Freq), percent = sum(count$percent))
  count <- rbind(count, newRow)
  colnames(count)[3] <- paste(unique(newdata$strWORep),"")
  
  extCount <- data.frame(t(count[-1]))
  colnames(extCount) <- count[, 1]
  
  return(extCount)
}

agreementPercent = data.frame()

for (i in 1:8 ) {
  agree <- Agreement.PercentWORep(data[which(data$indiceWORep ==i & data$outlier == 1),])
  agreementPercent <- rbind(agreementPercent, agree[2,])
}








#########################################################
### reading in data and transform it to matrix format
#########################################################
library(gplots)
library(RColorBrewer)

tableData <- agreementPercent
rnames <- tableData[,1]                            # assign labels in column 1 to "rnames"
mat_data <- data.matrix(tableData[,1:ncol(tableData)])  # transform column 2-5 into a matrix
rownames(mat_data) <- rownames(agreementPercent)                  # assign row names


#########################################################
### customizing and plotting heatmap
#########################################################



# creates a own color palette from red to green
my_palette <- colorRampPalette(c("red", "yellow", "green"))(n = 299)

# (optional) defines the color breaks manually for a "skewed" color transition
col_breaks = c(seq(0,.1,length=100),   # for red
               seq(0.11,0.2,length=100),            # for yellow
               seq(0.21,1,length=100))              # for green

col_breaks = c(seq(0,0.15,length=100),   # for red
               seq(0.16,0.33,length=100),            # for yellow
               seq(0.34,1,length=100))              # for green

heatmap.2(mat_data,
          cellnote = mat_data,  # same data set for cell labels
          main = "Correlation", # heat map title
          notecol="black",      # change font color of cell labels to black
          trace="none",         # turns off trace lines inside the heat map
          col=my_palette,       # use on color palette defined earlier
          breaks=col_breaks,    # enable color transition at specified limits
          Colv="NA")            # turn off column clustering










############################
### static
############################





agreementStaticPercent = data.frame()

for (i in 9:12) {
  agree <- Agreement.PercentWORep(dataStatic[which(dataStatic$indiceWORep ==i & dataStatic$outlier == 1),])
  agreementStaticPercent <- rbind(agreementStaticPercent, agree[2,])
}

agreementStaticPercent




agreementStaticPercent = data.frame()

for (i in 25:32) {
  agree <- Agreement.Percent(dataStatic[which(dataStatic$indice ==i & dataStatic$outlier == 1),])
  agreementStaticPercent <- rbind(agreementStaticPercent, agree[2,])
}

agreementStaticPercent



tableData <- agreementStaticPercent
rnames <- tableData[,1]                            # assign labels in column 1 to "rnames"
mat_data <- data.matrix(tableData[,1:ncol(tableData)])  # transform column 2-5 into a matrix
rownames(mat_data) <- rownames(agreementStaticPercent)                  # assign row names


#########################################################
### customizing and plotting heatmap
#########################################################

library(gplots)
library(RColorBrewer)


dev.off()

# creates a own color palette from red to green
my_palette <- colorRampPalette(c("red", "yellow", "green"))(n = 299)

# (optional) defines the color breaks manually for a "skewed" color transition
col_breaks = c(seq(0,.1,length=100),   # for red
               seq(0.11,0.2,length=100),            # for yellow
               seq(0.21,1,length=100))              # for green

col_breaks = c(seq(0,0.15,length=100),   # for red
               seq(0.16,0.33,length=100),            # for yellow
               seq(0.34,1,length=100))              # for green

heatmap.2(mat_data,
          cellnote = mat_data,  # same data set for cell labels
          main = "Correlation", # heat map title
          notecol="black",      # change font color of cell labels to black
          trace="none",         # turns off trace lines inside the heat map
          col=my_palette,       # use on color palette defined earlier
          breaks=col_breaks,    # enable color transition at specified limits
          Colv="NA")            # turn off column clustering






