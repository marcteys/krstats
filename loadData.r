library(plyr)
library(dplyr)
library(mvoutlier)

data = data.frame()
dataStatic = data.frame()
dataMeanPerTrialWORep = data.frame()
dataMeanPerTrialWORep = data.frame()
dataMedianPerTrialWORep = data.frame()
dataMedianPerTrialWORep = data.frame()
dataMeanPerTrialTotalWORep = data.frame()
dataMeanPerTrialTotalWORep = data.frame()
dataMedianPerTrialTotalWORep = data.frame()
dataMedianPerTrialTotalWORep = data.frame()
dataMeanPerTrial = data.frame()
dataMeanPerTrial = data.frame()
dataMedianPerTrial = data.frame()
dataMedianPerTrial = data.frame()
dataMeanPerTrialTotal = data.frame()
dataMeanPerTrialTotal = data.frame()
dataMedianPerTrialTotal = data.frame()
dataMedianPerTrialTotal = data.frame()
dataStaticMeanPerTrialWORep = data.frame()
dataStaticMeanPerTrialWORep = data.frame()
dataStaticMedianPerTrialWORep = data.frame()
dataStaticMedianPerTrialWORep = data.frame()
dataStaticMeanPerTrialTotalWORep = data.frame()
dataStaticMeanPerTrialTotalWORep = data.frame()
dataStaticMedianPerTrialTotalWORep = data.frame()
dataStaticMedianPerTrialTotalWORep = data.frame()
dataStaticMeanPerTrial = data.frame()
dataStaticMeanPerTrial = data.frame()
dataStaticMedianPerTrial = data.frame()
dataStaticMedianPerTrial = data.frame()
dataStaticMeanPerTrialTotal = data.frame()
dataStaticMeanPerTrialTotal = data.frame()
dataStaticMedianPerTrialTotal = data.frame()
dataStaticMedianPerTrialTotal = data.frame()







## Remove outliers
library(mvoutlier)

RemoveOutliers <- function(data, method) {
  if(method == "wilks") {
    dat.rows <- Wilks.function(data[,c("arousal","valence","indice")]);
    data$outlier <-  ifelse(dat.rows[,"p"] < 0.05 , 0, 1)
    #dataOutliers <- subset(data, globalOutlier == 1)
  } else {
    transformedData = data[,c("arousal","valence","indice")]
    transformedData$arousal = data$arousal + 4
    transformedData$valence = data$valence + 4
    result <- pcout(transformedData, qqplot = FALSE, method = "quan")
    data$outlier = result$wfinal01
  }
  print(paste("Removing outliers : ",nrow(data) -sum(data$outlier)))
  return(data)
}


Wilks.function <- function(dat){
  n <- nrow(dat)
  p <- ncol(dat)
  # beta distribution
  u <- n * mahalanobis(dat, center = colMeans(dat), cov = cov(dat))/(n-1)^2
  w <- 1 - u
  F.stat <- ((n-p-1)/p) * (1/w-1) # computing F statistic
  p <- 1 - round( pf(F.stat, p, n-p-1), 3) # p value for each row
  cbind(w, F.stat, p)
}

loadData <- function(){
  data <- read.csv("mov.csv",sep=",",header= TRUE)
  data$amplitude = as.factor(data$amplitude)
  data$force = as.factor(data$force)
  data$repetition = as.factor(data$repetition)
  data$speed = as.factor(data$speed)
  data$touchduration = as.factor(data$touchduration)
  data$subject = as.factor(data$subject)
  data$emotionIndex = as.factor(data$emotion)
  #data$valence = as.numeric(data$valence)
  #data$arousal = as.numeric(data$arousal)

  
  data$quadrant = gsub("0", "BottomLeft", data$emotion)
  data$quadrant = gsub("1", "TopLeft", data$quadrant)
  data$quadrant = gsub("2", "BottomLeft", data$quadrant)
  data$quadrant = gsub("3", "TopRight", data$quadrant)
  data$quadrant = gsub("4", "TopLeft", data$quadrant)
  data$quadrant = gsub("5", "BottomRight", data$quadrant)
  data$quadrant = gsub("6", "I don't know", data$quadrant)
  data$quadrant = gsub("7", "TopRight", data$quadrant)
  data$quadrant = gsub("8", "BottomRight", data$quadrant)
  
  data$Disgust = ifelse(data$emotion == 0 , 1, 0) 
  data$Angry = ifelse(data$emotion == 1 , 1, 0) 
  data$Sad = ifelse(data$emotion == 2 , 1, 0) 
  data$Happy = ifelse(data$emotion == 3 , 1, 0) 
  data$Fear = ifelse(data$emotion == 4 , 1, 0) 
  data$Interest = ifelse(data$emotion == 5 , 1, 0) 
  data$IDK = ifelse(data$emotion == 6 , 1, 0) 
  data$Surprise = ifelse(data$emotion == 7 , 1, 0) 
  data$Calm = ifelse(data$emotion == 8 , 1, 0) 
  
  
  
  
  data$emotion = gsub("0", "Disgust", data$emotion)
  data$emotion = gsub("1", "Angry", data$emotion)
  data$emotion = gsub("2", "Sad", data$emotion)
  data$emotion = gsub("3", "Happy", data$emotion)
  data$emotion = gsub("4", "Fear", data$emotion)
  data$emotion = gsub("5", "Interest", data$emotion)
  data$emotion = gsub("6", "I don't know", data$emotion)
  data$emotion = gsub("7", "Surprise", data$emotion)
  data$emotion = gsub("8", "Calm", data$emotion)

  
  data$expectedArousal <- gsub("Degout", 0.5, data$emotion)
  data$expectedValence <- gsub("Degout", -2, data$emotion)
  
  data$expectedArousal <- gsub("Colere", 1.8, data$expectedArousal)
  data$expectedValence <- gsub("Colere", -1.5, data$expectedValence)
  
  data$expectedArousal <- gsub("Tristesse", -0.8, data$expectedArousal)
  data$expectedValence <- gsub("Tristesse", -1.9, data$expectedValence)
  
  data$expectedArousal <- gsub("Joie", 1.6, data$expectedArousal)
  data$expectedValence <- gsub("Joie", 1.8, data$expectedValence)
  
  data$expectedArousal <- gsub("Peur", 2, data$expectedArousal)
  data$expectedValence <- gsub("Peur", -0.7, data$expectedValence)
  
  data$expectedArousal <- gsub("Interet", 0, data$expectedArousal)
  data$expectedValence <- gsub("Interet", 1.5, data$expectedValence)
  
  data$expectedArousal <- gsub("Je ne sais pas", 0, data$expectedArousal)
  data$expectedValence <- gsub("Je ne sais pas", 0, data$expectedValence)
  
  data$expectedArousal <- gsub("Surprise", 2, data$expectedArousal)
  data$expectedValence <- gsub("Surprise", 0, data$expectedValence)
  
  data$expectedArousal <- gsub("Calme", -1, data$expectedArousal)
  data$expectedValence <- gsub("Calme", 1.2, data$expectedValence)
  
  data$emotion = as.factor(data$emotion)

  return(data)
}


data = loadData()
summary(data)






###Treat the normal data

## Add string defining the touch
##### J'avais invers� !!
data$speedWord = gsub("16", "V+", data$speed) #ici on inverse parce que je suis un gland
data$speedWord = gsub("3.8", "V-", data$speedWord)

data$amplitudeWord = gsub("20", "A+", data$amplitude)
data$amplitudeWord = gsub("5", "A-", data$amplitudeWord)


data$forceWord = gsub("0", "F-", data$force)
data$forceWord = gsub("1", "F+", data$forceWord)

data$repetitionWord = gsub("0", "T0", data$repetition)
data$repetitionWord = gsub("1", "Ts", data$repetitionWord)
data$repetitionWord = gsub("2", "Tp", data$repetitionWord)

data$touchduration = gsub("1.315789474", "1.25", data$touchduration)


data$durationWord = gsub("1.25", "D1", data$touchduration)
data$durationWord = gsub("0.3125", "D0", data$durationWord)
data$durationWord = gsub("1.315789474", "D1", data$durationWord)
data$durationWord = gsub("5.263157895", "D2", data$durationWord)


#data$parameterID <- do.call(paste0, data[c("repetitionWord", "forceWord", "amplitudeWord", "speedWord")])
# data$parameterIDWORep <- do.call(paste0, data[c( "forceWord", "amplitudeWord", "speedWord")])



data$str = paste(data$speedWord,"",data$amplitudeWord,"",data$forceWord,"",data$repetitionWord, sep="") 
t = as.factor(data$str)
t = as.numeric(t)
data$indice = t

data <- RemoveOutliers(data,"wilks")
data$strWORep = paste(data$speedWord,"",data$amplitudeWord,"",data$forceWord, sep="") 
t = as.factor(data$strWORep)
t = as.numeric(t)
data$indiceWORep = t

dataMeanPerTrial = ddply(data, .(amplitude, speed, force, repetition, subject, str, strWORep,indiceWORep), summarize,  arousal=mean(arousal), valence=mean(valence))
dataMedianPerTrial = ddply(data, .(amplitude, speed, force, repetition, subject, str, strWORep,indiceWORep), summarize,  arousal=median(arousal), valence=median(valence))
dataMeanPerTrialTotal = ddply(data, .(amplitude, speed, force, repetition, str, strWORep,indiceWORep), summarize,  arousal=mean(arousal), valence=mean(valence))
dataMedianPerTrialTotal = ddply(data, .(amplitude, speed, force, repetition, str, strWORep,indiceWORep), summarize,  arousal=median(arousal), valence=median(valence))


dataMeanPerTrialWORep = ddply(data, .(amplitude, speed, force, subject, strWORep,indiceWORep), summarize,  arousal=mean(arousal), valence=mean(valence))
dataMedianPerTrialWORep = ddply(data, .(amplitude, speed, force, subject, strWORep,indiceWORep), summarize,  arousal=median(arousal), valence=median(valence))
dataMeanPerTrialTotalWORep = ddply(data, .(amplitude, speed, force, strWORep,indiceWORep), summarize,  arousal=mean(arousal), valence=mean(valence))
dataMedianPerTrialTotalWORep = ddply(data, .(amplitude, speed, force,  strWORep,indiceWORep), summarize,  arousal=median(arousal), valence=median(valence))

#### 


dataMeanPerTrialWORep = ddply(subset(data, outlier ==1), .(amplitude, speed, force, subject, strWORep,indiceWORep), summarize,  arousal=mean(arousal), valence=mean(valence))
dataMedianPerTrialWORep = ddply(subset(data, outlier ==1), .(amplitude, speed, force, subject, strWORep,indiceWORep), summarize,  arousal=median(arousal), valence=median(valence))
dataMeanPerTrialTotalWORep = ddply(subset(data, outlier ==1), .(amplitude, speed, force, strWORep,indiceWORep), summarize,  arousal=mean(arousal), valence=mean(valence))
dataMedianPerTrialTotalWORep = ddply(subset(data, outlier ==1), .(amplitude, speed, force,  strWORep,indiceWORep), summarize,  arousal=median(arousal), valence=median(valence))



exp = merge(dataMeanPerTrialWORep, users, by=c("subject"))
write.csv(exp,"dataWOREP.csv")




















### Treat the repetition Static data


loadDataStatic <- function(){
  data <- read.csv("static.csv",sep=",",header= TRUE)
  data$duration = as.factor(data$duration)
  data$force = as.factor(data$force)
  data$repetition = as.factor(data$repetition)
  data$touchdDuration = as.factor(data$touchDuration)
  data$subject = as.factor(data$subject)
  #data$valence = as.numeric(data$valence)
  #data$arousal = as.numeric(data$arousal)
  
  data$Disgust = ifelse(data$emotion == 0 , 1, 0) 
  data$Angry = ifelse(data$emotion == 1 , 1, 0) 
  data$Sad = ifelse(data$emotion == 2 , 1, 0) 
  data$Happy = ifelse(data$emotion == 3 , 1, 0) 
  data$Fear = ifelse(data$emotion == 4 , 1, 0) 
  data$Interest = ifelse(data$emotion == 5 , 1, 0) 
  data$IDK = ifelse(data$emotion == 6 , 1, 0) 
  data$Surprise = ifelse(data$emotion == 7 , 1, 0) 
  data$Calm = ifelse(data$emotion == 8 , 1, 0) 
  
  
  
  data$quadrant = gsub("0", "BottomLeft", data$emotion)
  data$quadrant = gsub("1", "TopLeft", data$quadrant)
  data$quadrant = gsub("2", "BottomLeft", data$quadrant)
  data$quadrant = gsub("3", "TopRight", data$quadrant)
  data$quadrant = gsub("4", "TopLeft", data$quadrant)
  data$quadrant = gsub("5", "BottomRight", data$quadrant)
  data$quadrant = gsub("6", "I don't know", data$quadrant)
  data$quadrant = gsub("7", "TopRight", data$quadrant)
  data$quadrant = gsub("8", "BottomRight", data$quadrant)
  
  
  data$emotion = gsub("0", "Disgust", data$emotion)
  data$emotion = gsub("1", "Angry", data$emotion)
  data$emotion = gsub("2", "Sad", data$emotion)
  data$emotion = gsub("3", "Happy", data$emotion)
  data$emotion = gsub("4", "Fear", data$emotion)
  data$emotion = gsub("5", "Interest", data$emotion)
  data$emotion = gsub("6", "I don't know", data$emotion)
  data$emotion = gsub("7", "Surprise", data$emotion)
  data$emotion = gsub("8", "Calm", data$emotion)
  
  
  data$emotion = gsub("0", "Degout", data$emotion)
  data$emotion = gsub("1", "Colere", data$emotion)
  data$emotion = gsub("2", "Tristesse", data$emotion)
  data$emotion = gsub("3", "Joie", data$emotion)
  data$emotion = gsub("4", "Peur", data$emotion)
  data$emotion = gsub("5", "Interet", data$emotion)
  data$emotion = gsub("6", "Je ne sais pas", data$emotion)
  data$emotion = gsub("7", "Surprise", data$emotion)
  data$emotion = gsub("8", "Calme", data$emotion)
  data$emotion = as.factor(data$emotion)
  
  return(data)
}
dataStatic = loadDataStatic()
summary(dataStatic)

#arousal = summarySE(dataStatic, "arousal", c("subject","touchdDuration","force","repetition"))
#valence = summarySE(dataStatic, "valence", c("subject","touchDuration","force","repetition"))
#concatenatedTableau = merge(arousal, valence, by=c("subject","touchDuration","force","repetition"))








###Treat the normal dataStatic

## Add string defining the touch
dataStatic$durationword = gsub("0", "D-", dataStatic$duration)
dataStatic$durationword = gsub("1", "D+", dataStatic$durationword)

dataStatic$forceWord = gsub("0", "F-", dataStatic$force)
dataStatic$forceWord = gsub("1", "F+", dataStatic$forceWord)

dataStatic$repetitionWord = gsub("0", "T0", dataStatic$repetition)
dataStatic$repetitionWord = gsub("1", "Ts", dataStatic$repetitionWord)
dataStatic$repetitionWord = gsub("2", "Tp", dataStatic$repetitionWord)


dataStatic$tdurationWord = gsub("0.3125", "D0", dataStatic$touchDuration)
dataStatic$tdurationWord = gsub("1.3000", "D1", dataStatic$tdurationWord)
dataStatic$tdurationWord = gsub("5.2000", "D2", dataStatic$tdurationWord)

dataStatic$str = paste(dataStatic$durationword,"/",dataStatic$forceWord,"/",dataStatic$repetitionWord, sep="") 
t = as.factor(dataStatic$str)
t = as.numeric(t)
dataStatic$indice = t + 24


dataStatic <- RemoveOutliers(dataStatic,"wilks")
dataStatic$strWORep = paste(dataStatic$durationword,"/",dataStatic$forceWord, sep="") 

t = as.factor(dataStatic$strWORep)
t = as.numeric(t)
dataStatic$indiceWORep = t + 8


dataStaticMeanPerTrial = ddply(dataStatic, .(duration, force, repetition, subject, str, strWORep), summarize,  arousal=mean(arousal), valence=mean(valence))
dataStaticMedianPerTrial = ddply(dataStatic, .(duration, force, repetition, subject, str, strWORep), summarize,  arousal=median(arousal), valence=median(valence))
dataStaticMeanPerTrialTotal = ddply(dataStatic, .(duration, force, repetition, str, strWORep), summarize,  arousal=mean(arousal), valence=mean(valence))
dataStaticMedianPerTrialTotal = ddply(dataStatic, .(duration, force, repetition, str, strWORep), summarize,  arousal=median(arousal), valence=median(valence))


dataStaticMeanPerTrialWORep = ddply(subset(dataStatic, outlier ==1), .( force, subject,  strWORep), summarize,  arousal=mean(arousal), valence=mean(valence))
dataStaticMedianPerTrialWORep = ddply(subset(dataStatic, outlier ==1), .( force, subject,  strWORep), summarize,  arousal=median(arousal), valence=median(valence))
dataStaticMeanPerTrialTotalWORep = ddply(subset(dataStatic, outlier ==1), .( force,  strWORep), summarize,  arousal=mean(arousal), valence=mean(valence))
dataStaticMedianPerTrialTotalWORep = ddply(subset(dataStatic, outlier ==1), .( force,  strWORep), summarize,  arousal=median(arousal), valence=median(valence))















loadUsers <- function(){
  user <- read.csv("users.csv",sep=",",header= TRUE)
  user$subject = as.factor(user$UserId)
  return(user)
}

users = loadUsers()
summary(users)








### Combining data




data = merge(data, users, by=c("subject"))
data$Gender = as.factor(data$Gender)


write.csv(data,"dataFull.csv")
write.csv(dataStatic,"dataStaticFull.csv")
