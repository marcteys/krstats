



library("irr")
sub <- data[,c("str","emotion")]
kappa2(sub, sort.levels = FALSE)


aaa <- summarySE(data = data, measurevar = "arousal", groupvars=c("subject", "strWORep"))
colnames(aaa)[5] = "sduser"
aaa
bbb <- summarySE(data = aaa, measurevar = "sduser", groupvars=c("N"))
bbb



aaa <- summarySE(data = data, measurevar = "arousal", groupvars=c("subject", "strWORep"))




# SAVING

test = summaryBy(arousal + valence ~ speed + subject, data=data)

fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())

fMoins = subset(test, speed == 3.8)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$arousal.mean)))

fPlus = subset(test, speed ==16)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$arousal.mean)))
fTTolal$names = c("fast speed", "slow speed")


fone = ggplot(fTTolal) +
  geom_pointrange(aes(y=V1, x=names, ymin=V2, ymax=V3,color=names))  +
  coord_flip() +
  labs(x = " ", y = "speed / arousal", color = "")  
fone

ggsave("speedarousal.pdf", fone,"pdf")





















# Est-ce que la DUREE a un effet sur l'arousal

test = summaryBy(valence + arousal ~ touchduration + subject, data=data)

fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
# Faire un autre tableau avec 
fMoins = subset(test, touchduration ==0.3125)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$arousal.mean)))

fPlus = subset(test, touchduration ==1.25)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$arousal.mean)))


fPlus = subset(test, touchduration ==1.315789474)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$arousal.mean)))


fPlus = subset(test, touchduration ==5.263157895)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$arousal.mean)))

fTTolal$names = c("0.3", "1.25", "1.3", "5.2")

fdure = ggplot(fTTolal) + geom_pointrange(aes(y=V1, x=names, ymin=V2, ymax=V3)) + coord_flip() +
  labs(x = "duration / arousal ", y = "", color = "") 

fdure






fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
# Faire un autre tableau avec 
fMoins = subset(test, touchduration ==0.3125)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$valence.mean)))

fPlus = subset(test, touchduration ==1.25)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$valence.mean)))


fPlus = subset(test, touchduration ==1.315789474)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$valence.mean)))


fPlus = subset(test, touchduration ==5.263157895)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$valence.mean)))

fTTolal$names = c("0.3", "1.25", "1.3", "5.2")

fdure = ggplot(fTTolal) + geom_pointrange(aes(y=V1, x=names, ymin=V2, ymax=V3, color=names)) + coord_flip() +
  labs(x = "duration / valence ", y = "", color = "") 

fdure










style_confidence <- function(title = "") {
  style <- list (
    coord_flip(ylim=c(-1,1.5), xlim=c(0,3), expand = FALSE),
    theme_light(),
    geom_pointrange(aes(y=V1, x=names, ymin=V2, ymax=V3,color=names), shape=16, size=1),
    labs(x = "", y = title, color = ""),
    theme(),
    theme(legend.position='none',
          panel.border = element_blank(),
          strip.text=element_blank(),
        #  axis.text.y = element_blank()
        #  panel.grid.major.y =element_blank()
        #  , axis.line.y = element_line(colour = "grey")
          )
  )
  return(style)
}




# Est-ce que la FORCE a un effet sur l'arousal

test = summaryBy(valence + arousal ~ force + subject, data=data)

fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
# Faire un autre tableau avec 
fMoins = subset(test, force ==0)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$arousal.mean)))

fPlus = subset(test, force ==1)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$arousal.mean)))
#fDeltaValence <- fPlus$arousal.mean - fMoins$arousal.mean
#delta = t(meanCI.bootstrap(fDeltaValence))
#fTTolal = rbind(fTTolal, delta)
fTTolal$names = c("low force", "srong force")


fone = ggplot(fTTolal) + style_confidence("fa")
fone

# CE que veut dire deltaValence c'est que les utilisateurs notent toujours plus positiviement la force. La distance jusqu'a zero c'est le p
# On est sur que les gens indiquent des valeurs plus haut sur l'un que sur l'autre. Bcp de variation entre le sparticipants




# Est-ce que la force a un effet sur l'arousal

fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
# Faire un autre tableau avec 
fMoins = subset(test, force ==0)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$valence.mean)))

fPlus = subset(test, force ==1)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$valence.mean)))
fDeltaValence <- fPlus$valence.mean - fMoins$valence.mean

#delta = t(meanCI.bootstrap(fDeltaValence))
#fTTolal = rbind(fTTolal, delta)
fTTolal$names = c("low force", "strong force")

ftwo = ggplot(fTTolal) + style_confidence("fv")
ftwo





# SPEED

test = summaryBy(valence + arousal ~ speed + subject, data=data)

fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
# Faire un autre tableau avec 
fMoins = subset(test, speed ==3.8)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$arousal.mean)))

fPlus = subset(test, speed ==16)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$arousal.mean)))
fDeltaValence <- fPlus$arousal.mean - fMoins$arousal.mean

#delta = t(meanCI.bootstrap(fDeltaValence))
#fTTolal = rbind(fTTolal, delta)
fTTolal$names = c("low speed", "fast speed") 

speedone = ggplot(fTTolal) + style_confidence("sa")


test = summaryBy(valence + arousal ~ speed + subject, data=data)

fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
fMoins = subset(test, speed ==3.8)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$valence.mean)))

fPlus = subset(test, speed ==16)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$valence.mean)))
fDeltaValence <- fPlus$valence.mean - fMoins$valence.mean

#delta = t(meanCI.bootstrap(fDeltaValence))
#fTTolal = rbind(fTTolal, delta)
fTTolal$names = c("low speed", "fast speed") 

speedtwo = ggplot(fTTolal) + style_confidence("sv")










# DISTANCE

test = summaryBy(valence + arousal ~ amplitude + subject, data=data)

fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
# Faire un autre tableau avec 
fMoins = subset(test, amplitude ==5)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$arousal.mean)))

fPlus = subset(test, amplitude ==20)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$arousal.mean)))
fDeltaValence <- fPlus$arousal.mean - fMoins$arousal.mean

#delta = t(meanCI.bootstrap(fDeltaValence))
#fTTolal = rbind(fTTolal, delta)
fTTolal$names = c("short amplitude", "long amplitude")

distanceone = ggplot(fTTolal) +  style_confidence("aa")



fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
fMoins = subset(test, amplitude ==5)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$valence.mean)))

fPlus = subset(test, amplitude ==20)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$valence.mean)))
fDeltaValence <- fPlus$valence.mean - fMoins$valence.mean

#delta = t(meanCI.bootstrap(fDeltaValence))
#fTTolal = rbind(fTTolal, delta)
fTTolal$names = c("short amplitude", "long amplitude")

distancetwo = ggplot(fTTolal) + style_confidence("av")





# REPETITION

test = summaryBy(valence + arousal ~ repetition + subject, data=data)

fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
# Faire un autre tableau avec 
fMoins = subset(test, repetition ==0)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$arousal.mean)))

fPlus = subset(test, repetition ==1)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$arousal.mean)))
fDeltaValence <- fPlus$arousal.mean - fMoins$arousal.mean


fMid = subset(test, repetition ==2)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMid$arousal.mean)))
fDeltaValence <- fMid$arousal.mean - fMoins$arousal.mean


#delta = t(meanCI.bootstrap(fDeltaValence))
#fTTolal = rbind(fTTolal, delta)
fTTolal$names = c("no repetition", "pat", "stroke")

repetitionone = ggplot(fTTolal)  +  style_confidence("ra")

repetitionone




test = summaryBy(valence + arousal ~ repetition + subject, data=data)

fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
# Faire un autre tableau avec 
fMoins = subset(test, repetition ==0)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$valence.mean)))

fPlus = subset(test, repetition ==1)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$valence.mean)))
fDeltaValence <- fPlus$valence.mean - fMoins$valence.mean


fMid = subset(test, repetition ==2)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMid$valence.mean)))
fDeltaValence <- fMid$valence.mean - fMoins$valence.mean


#delta = t(meanCI.bootstrap(fDeltaValence))
#fTTolal = rbind(fTTolal, delta)
fTTolal$names = c("no repetition", "pat", "stroke")

repetitiontwo = ggplot(fTTolal) +  style_confidence("rv")

repetitiontwo


grid <- grid.arrange(fone, ftwo,distanceone, distancetwo,speedone, speedtwo,repetitionone, repetitiontwo, nrow = 4 , ncol = 2)
grid

#ggsave("grid.pdf", grid, "pdf")










grid.arrange(fone, ftwo, speedone, speedtwo, distanceone, distancetwo, repetitionone, repetitiontwo, ncol = 2)





#####??? Duration

test = summaryBy(valence + arousal ~ durationWord + subject, data=data)

fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
# Faire un autre tableau avec 
fMoins = subset(test, durationWord =="D0")
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$valence.mean)))

fPlus = subset(test, durationWord =="D1")
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$valence.mean)))
fDeltaValence <- fPlus$valence.mean - fMoins$valence.mean


fMid = subset(test, durationWord =="D2")
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMid$valence.mean)))
fDeltaValence <- fMid$valence.mean - fMoins$valence.mean


#delta = t(meanCI.bootstrap(fDeltaValence))
#fTTolal = rbind(fTTolal, delta)
fTTolal$names = c("d0", "d1", "d2")

duration = ggplot(fTTolal) +  style_confidence("rv")
duration




















################################################### STATIC







# Est-ce que la FORCE a un effet sur l'arousal

test = summaryBy(valence + arousal ~ force + subject, data=dataStatic)





fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
# Faire un autre tableau avec 
fMoins = subset(test, force ==0)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$arousal.mean)))

fPlus = subset(test, force ==1)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$arousal.mean)))
fDeltaArousal<- fPlus$arousal.mean - fMoins$arousal.mean

delta = t(meanCI.bootstrap(fDeltaArousal))
fTTolal = rbind(fTTolal, delta)
fTTolal$names = c("lowForce", "strongForce", "deltaArousal")

fone = ggplot(fTTolal) + geom_pointrange(aes(y=V1, x=names, ymin=V2, ymax=V3,color=names)) + coord_flip() +
  labs(x = "force / arousal ", y = "", color = "") 



# CE que veut dire deltaValence c'est que les utilisateurs notent toujours plus positiviement la force. La distance jusqu'a zero c'est le p
# On est sur que les gens indiquent des valeurs plus haut sur l'un que sur l'autre. Bcp de variation entre le sparticipants




# Est-ce que la force a un effet sur l'arousal

fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
# Faire un autre tableau avec 
fMoins = subset(test, force ==0)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$valence.mean)))

fPlus = subset(test, force ==1)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$valence.mean)))
fDeltaValence <- fPlus$valence.mean - fMoins$valence.mean

delta = t(meanCI.bootstrap(fDeltaValence))
fTTolal = rbind(fTTolal, delta)
fTTolal$names = c("lowForce", "strongForce", "deltaValence")

ftwo = ggplot(fTTolal) + geom_pointrange(aes(y=V1, x=names, ymin=V2, ymax=V3,color=names)) + coord_flip() +
  labs(x = "force / valence ", y = "", color = "") 



ftwo





# DURATION

test = summaryBy(valence + arousal ~ duration + subject, data=dataStatic)

fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
# Faire un autre tableau avec 
fMoins = subset(test, duration ==0)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$arousal.mean)))

fPlus = subset(test, duration ==1)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$arousal.mean)))
fDeltaArousal<- fPlus$arousal.mean - fMoins$arousal.mean

delta = t(meanCI.bootstrap(fDeltaArousal))
fTTolal = rbind(fTTolal, delta)
fTTolal$names = c("shortDuration", "longDuration", "deltaArousal")

distanceone = ggplot(fTTolal) + geom_pointrange(aes(y=V1, x=names, ymin=V2, ymax=V3,color=names)) + coord_flip() +
  labs(x = "Duration / arousal ", y = "", color = "") 


fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
fMoins = subset(test, duration ==0)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$valence.mean)))

fPlus = subset(test, duration ==1)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$valence.mean)))
fDeltaValence <- fPlus$valence.mean - fMoins$valence.mean

delta = t(meanCI.bootstrap(fDeltaValence))
fTTolal = rbind(fTTolal, delta)
fTTolal$names = c("shortDuration", "longDuration", "deltaArousal")

distancetwo = ggplot(fTTolal) + geom_pointrange(aes(y=V1, x=names, ymin=V2, ymax=V3, color=names)) + coord_flip() +
  labs(x = "Duration / valence ", y = "", color = "") 





# REPETITION

test = summaryBy(valence + arousal ~ repetition + subject, data=dataStatic)

fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
# Faire un autre tableau avec 
fMoins = subset(test, repetition ==0)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$arousal.mean)))

fPlus = subset(test, repetition ==1)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$arousal.mean)))
fDeltaValence <- fPlus$arousal.mean - fMoins$arousal.mean


fMid = subset(test, repetition ==2)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMid$arousal.mean)))
fDeltaValence <- fMid$arousal.mean - fMoins$arousal.mean


delta = t(meanCI.bootstrap(fDeltaValence))
fTTolal = rbind(fTTolal, delta)
fTTolal$names = c("no repetition", "pat", "stroke", "deltaArousal")

repetitionone = ggplot(fTTolal) + geom_pointrange(aes(y=V1, x=names, ymin=V2, ymax=V3,color=names)) + coord_flip() +
  labs(x = "Repetition / varousal ", y = "", color = "") 




test = summaryBy(valence + arousal ~ repetition + subject, data=data)

fTTolal = data.frame(estimate = numeric(), low = numeric(), high = numeric())
# Faire un autre tableau avec 
fMoins = subset(test, repetition ==0)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMoins$valence.mean)))

fPlus = subset(test, repetition ==1)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fPlus$valence.mean)))
fDeltaValence <- fPlus$valence.mean - fMoins$valence.mean


fMid = subset(test, repetition ==2)
fTTolal <- rbind(fTTolal, t(meanCI.bootstrap(fMid$valence.mean)))
fDeltaValence <- fMid$valence.mean - fMoins$valence.mean


delta = t(meanCI.bootstrap(fDeltaValence))
fTTolal = rbind(fTTolal, delta)
fTTolal$names = c("no repetition", "pat", "stroke", "deltaValence")

repetitiontwo = ggplot(fTTolal) + geom_pointrange(aes(y=V1, x=names, ymin=V2, ymax=V3,color=names)) + coord_flip() +
  labs(x = "Repetition / valence ", y = "", color = "") 







grid.arrange(fone, ftwo,distanceone, distancetwo, ncol = 2)






















### Confidence pour confidence





allGraph = summaryBy(valence + arousal ~ strWORep + subject + indiceWORep , data=data)
allConfidence= data.frame(estimate = numeric(), low = numeric(), high = numeric(), estimate = numeric(), low = numeric(), high = numeric(), str = factor(), indice = numeric())


for(i in 1:8) {
  sub = subset(allGraph, indiceWORep ==i)
  uniqueStr = unique(sub$strWORep)
  uniqueStr
  
  #tmpTable$str = uniqueStr
  #allConfidence[nrow(allConfidence) + 1,] = c(tmpTable,uniqueStr)
  tmp = as.numeric(meanCI.bootstrap(sub$arousal.mean))
  tmp2 = as.numeric(meanCI.bootstrap(sub$valence.mean))
  tmp[4] = tmp2[1]
  tmp[5] = tmp2[2]
  tmp[6] = tmp2[3]
  
  tmp[7] = uniqueStr
  tmp[8] = i
  
  allConfidence <- rbind.data.frame(allConfidence,  t(tmp))
}

allConfidence$V1 = as.numeric(as.character(allConfidence$V1))
allConfidence$V2 = as.numeric(as.character(allConfidence$V2))
allConfidence$V3 = as.numeric(as.character(allConfidence$V3))
allConfidence$V4 = as.numeric(as.character(allConfidence$V4))
allConfidence$V5 = as.numeric(as.character(allConfidence$V5))
allConfidence$V6 = as.numeric(as.character(allConfidence$V6))

colnames(allConfidence)[8] = "indiceWORep"



confidenceGraph = ggplot(allConfidence) + 
  geom_pointrange(aes(y=V1, x=V7, ymin=V2, ymax=V3,color=V7)) +
  coord_flip(ylim=c(-2,2), xlim=c(0,25), expand = FALSE) +
  theme_light() + theme(legend.position = "none") + labs(x = "", y = "", color = "")
confidenceGraph






toEllipse <- function(data) {
 # for(i in 1:nrow(data)) {
    ellipseTmp = data.frame(low = numeric(), center = numeric(), high = numeric())
    ellipseTmp[nrow(ellipseTmp)+1,] <- c(data$V2,data$V1,data$V3)
    ellipseTmp[nrow(ellipseTmp)+1,] <- c(data$V5,data$V4, data$V6)
    rownames(ellipseTmp) <- c("x","y")
    #}
    
    el <- data.frame(anellipse(ellipseTmp))
    return(el)
}


ggplot_ellipsoid <- function(allData) {
  
    df <- data.frame(replicate(2,sample(-10000:10000,nrow(allData),rep=TRUE)))
    df$X1 = as.factor((df$X1))
    df$X2 = as.factor((df$X2))
    p <- ggplot(df, aes(x = X1, y = X2, color = X1)) + geom_point(shape = 21, size = nrow(allData))
  col <- as.data.frame(ggplot_build(p)$data)$colour
col <- as.matrix(col)

  myggplotEllipses <- list()  # new empty list
  for (i in 1:nrow(allData)) {
    myEllipse <- toEllipse(allConfidence[i,])
    ggploEllipse = ggplot_ellipsoid_item(myEllipse, color = as.character(col[i,1]) )
    myggplotEllipses[[i]] <- ggploEllipse  # add each plot into plot list
  }
  return(myggplotEllipses)
}



ggplot_ellipsoid_item <- function(poly, color = "red") {
  print(color)
  return(list(geom_polygon(data=poly, aes(x=y, y=x, alpha = 0.5),  fill = color, color = color)))
}





ellipsoide <- function(allData) {
  aa  = data.frame()
  myggplotEllipses <- list()  # new empty list
  
  for (i in 1:nrow(allData)) {
    myEllipse <- toEllipse(allConfidence[i,])
    aa <- rbind(aa, myEllipse)
    myggplotEllipses[[i]] <- myEllipse  # add each plot into plot list
  }
  return(aa)
}


poly_tm <- data.frame(
  x=c(-0.5, 0.5, 0.5, -0.5),
  y=c(2, 2, 3, 3),
  fill=rep("D", 4)
)




#df <- subset(data, outliers == 1)
find_circle <- function(d) {
  toEllipse(d)
}


find_circle(allConfidence)
#patatoides <- ddply(allConfidence, "str", find_circle)
df

plot <- ggplot(data = df, aes(x = valence, y = arousal, colour=str, fill = str)) +
  geom_point() + 
  geom_jitter(width = 0.1, height = 0.1) +
  # geom_text(data = data, aes( label = trial), hjust = 2) + 
  labs(x = "Valence", y = "Arousal")
plot <- ggplotly(plot)
plot



allConfidence_ellipse <- ddply(allConfidence, "V7", find_circle)

confidenceGraph = ggplot(allConfidence) + 
  geom_polygon(data = allConfidence_ellipse, aes(y=x, x=y, color = V7), alpha = 0.1) +
  geom_simple_polygon(aes(y=V1, x=V4, color = V7))+
  geom_point(aes(y=V1, x=V4, color = V7)) +
  geom_segment(mapping=aes(y=V2, x=V4, yend=V3, xend=V4, color = V7)) + 
  geom_segment(mapping=aes(y=V1, x=V5, yend=V1, xend=V6, color = V7)) + 
  theme_light() + theme(legend.position = "right") + labs(x = "", y = "", color = "") +
  coord_cartesian(ylim=c(-2,2),xlim=c(-2,2) ) + coord_fixed()  + theme(aspect.ratio=1)
confidenceGraph



confidenceGraph <- ggplotly(confidenceGraph)
confidenceGraph






api_create(confidenceGraph, filename = "Points avec Cercles",fileopt = "overwrite", sharing = "public")


